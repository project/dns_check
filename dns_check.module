<?php

/**
 * @file
 * Module to check email fields have resolvable DNS.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 *
 * @param $route_name
 * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
 *
 * @return string
 */
function dns_check_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name == 'help.page.dns_check') {
      $output = '';
      $output = '<h3>' . t('DNS Check') . '</h3>';
      $output = '<p>' . t('Provides validation for user emails.') . '</p>';
      $output = '<p>' . t('Can optionally validate email type fields in additional forms.') . '</p>';
      return $output;
  }

}

/**
 * Implements hook_entity_base_field_info_alter.
 *
 * @param $fields
 * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
 */
function dns_check_entity_base_field_info_alter(&$fields, EntityTypeInterface $entity_type) {
  // Alter the mail field to be checked for a valid DNS domain.
  if ($entity_type->id() == 'user' && !empty($fields['mail'])) {
    if (\Drupal::config('dns_check.settings')
        ->get('validate_user_emails') == '1') {
      $fields['mail']->addConstraint('DnsDomainExists', []);
    }
  }

}
