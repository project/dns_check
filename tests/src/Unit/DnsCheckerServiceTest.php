<?php

namespace Drupal\Tests\dns_check\Unit;

use Drupal\dns_check\Service\DnsCheckerService;
use Drupal\Tests\UnitTestCase;
use Symfony\Bridge\PhpUnit\DnsMock;

/**
 * DNSChecker unit tests.
 *
 * @ingroup dns_check
 * @group dns_check
 * @group dns-sensitive
 *
 * @coversDefaultClass \Drupal\dns_check\Service\DnsCheckerService
 */
class DnsCheckerServiceTest extends UnitTestCase {

  /**
   * Test constructor works.
   */
  public function testConstructor() {
    $dnsChecker = new DnsCheckerService();

    $this->assertInstanceOf(DnsCheckerService::class, $dnsChecker);
  }

  /**
   * Test DNS lookup.
   */
  public function testDnsValidation() {

    DnsMock::register(DnsCheckerService::class);
    DnsMock::withMockedHosts(['gmailforrealz.com' => [['type' => 'MX']]]);

    $dnsChecker = new DnsCheckerService();

    // Test a valid host.
    $result = $dnsChecker->validateHost('gmailforrealz.com');
    $this->assertEquals($dnsChecker::VALID, $result, 'A valid host could not be resolved.');

    // Test an invalid host.
    $result = $dnsChecker->validateHost('gmalforrealz.co');
    $this->assertEquals($dnsChecker::NX_DOMAIN, $result, 'An invalid host could be resolved.');
  }

  /**
   * Test email validation.
   */
  public function testEmailValidation() {

    DnsMock::register(DnsCheckerService::class);
    DnsMock::withMockedHosts(['gmailforrealz.com' => [['type' => 'MX']]]);

    $dnsChecker = new DnsCheckerService();

    // Test an invalid email format.
    $result = $dnsChecker->validateEmail('testgmailforrealz.com');
    $this->assertEquals($dnsChecker::INVALID_EMAIL, $result, 'An invalid email passed the test.');

    // Test a non-existent host.
    $result = $dnsChecker->validateEmail('test@gmalforrealz.co');
    $this->assertEquals($dnsChecker::NX_DOMAIN, $result, 'An email from an invalid host still resolved.');

    // Test a valid host.
    $result = $dnsChecker->validateEmail('test@gmailforrealz.com');
    $this->assertEquals($dnsChecker::VALID, $result, 'An email from a valid host did not resolve.');
  }

}
