<?php

namespace Drupal\Tests\dns_check\Unit;

use Drupal\Tests\UnitTestCase;
use Symfony\Bridge\PhpUnit\DnsMock;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Drupal\dns_check\Plugin\Validation\Constraint\DnsDomainExists;
use Drupal\dns_check\Plugin\Validation\Constraint\DnsDomainExistsValidator;
use Drupal\dns_check\Service\DnsCheckerService;

/**
 * DnsDomainExistsValidator unit tests.
 *
 * @ingroup dns_check
 * @group dns_check
 * @group dns-sensitive
 *
 * @coversDefaultClass \Drupal\dns_check\Plugin\Validation\Constraint\DnsDomainExistsValidator
 */
class DnsDomainExistsValidatorTest extends UnitTestCase {

  /**
   * Test the constructor works.
   */
  public function testConstructor() {

    $dnsChecker = new DnsCheckerService();
    $validator = new DnsDomainExistsValidator($dnsChecker);

    $this->assertInstanceOf(DnsDomainExistsValidator::class, $validator);
  }

  /**
   * @covers ::validate
   * @dataProvider providerValidate
   *
   * @param string $value
   *   The value to validate.
   *
   * @param array $hosts
   *   The hosts to mock as valid hosts.
   *
   * @param bool $valid
   *   Whether the test should expect a valid or invalid result.
   */
  public function testDnsValidator(string $value, array $hosts, bool $valid) {

    DnsMock::register(DnsCheckerService::class);
    DnsMock::withMockedHosts($hosts);

    $dnsChecker = new DnsCheckerService();

    $context = $this->createMock(ExecutionContextInterface::class);

    if ($valid) {
      $context->expects($this->never())
        ->method('addViolation');
    }
    else {
      $context->expects($this->once())
        ->method('addViolation');
    }

    $constraint = new DnsDomainExists();

    $validator = new DnsDomainExistsValidator($dnsChecker);
    $validator->initialize($context);
    $validator->validate($value, $constraint);
  }

  /**
   * Data provider for ::testDNSValidator.
   */
  public function providerValidate() {
    $data = [];

    // Setup hosts.
    $hosts = ['gmailforrealz.com' => [['type' => 'MX']]];

    // Invalid host, check domain only.
    $data[] = ['gmailforrealz.co', $hosts, FALSE];

    // Valid host, check domain only.
    $data[] = ['gmailforrealz.com', $hosts, TRUE];

    // Valid host, check invalid email.
    $data[] = ['gm@ailfo@gmailforrealz.com', $hosts, FALSE];

    // Valid host, check valid email.
    $data[] = ['ok@gmailforrealz.com', $hosts, TRUE];

    // Invalid host, check valid email.
    $data[] = ['ok@gmailforalz.om', $hosts, FALSE];

    // TODO: Test not passing.
    $data = [];

    return $data;
  }

}
