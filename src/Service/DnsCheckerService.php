<?php

namespace Drupal\dns_check\Service;

use Drupal\Component\Utility\Html;

/**
 * Class DnsCheckerService.
 *
 * @package Drupal\dns_check\Service
 */
class DnsCheckerService {

  public const INVALID_EMAIL = 1;
  public const NX_DOMAIN = 2;
  public const VALID = 3;

  /**
   * Validate an email using email format and valid domain.
   *
   * @param string $email
   *   An email to check.
   *
   * @return int
   *   A class constant indicating success or error.
   */
  public function validateEmail(string $email) : int {
    // Get the email.
    $mail_safe = Html::escape($email);
    $valid_email = filter_var($mail_safe, FILTER_VALIDATE_EMAIL);
    if (!$valid_email) {
      return self::INVALID_EMAIL;
    }
    $mail = explode('@', $mail_safe);
    return $this->validateHost(end($mail));
  }

  /**
   * Validate a domain exists.
   *
   * @param string $host
   *   A host to check.
   *
   * @return int
   *   A class constant indicating success or error.
   */
  public function validateHost(string $host) : int {

    $result = self::VALID;
    $host_safe = Html::escape($host);
    // Fetch DNS Resource Records associated with a hostname.
    $records = dns_get_record($host_safe, DNS_MX);
    if (empty($records)) {
      $result = self::NX_DOMAIN;
    }
    return $result;
  }

}
