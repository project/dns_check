<?php

namespace Drupal\dns_check\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DnsCheckConfigurationAdminForm.
 *
 * @package Drupal\dns_check\Form
 */
class DnsCheckConfigurationAdminForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'dns_check.settings';

  /**
   * The logger to use.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              LoggerChannelInterface $loggerChannel) {

    parent::__construct($config_factory);
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('dns_check.logger.channel.dns_check')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dns_check.validate_user_emails',
      'dns_check.validate_additional_forms',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'dns_check_configuration_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['validate_user_emails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate the DNS domains of user emails?'),
      '#description' => $this->t('Should valid DNS be enforced for user emails? Validation is also performed on REST API.'),
      '#default_value' => $config->get('validate_user_emails'),
    ];

    $additional = $config->get('validate_additional_forms');
    $form['validate_additional_forms'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Validate additional forms with email fields using DNS"),
      '#description' => $this->t(
        "Enter additional form ids to validate. If they have email fields they will be required to have valid DNS."
      ),
      '#default_value' => implode("\n", $additional),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values['validate_user_emails'] = $form_state->getValue('validate_user_emails');
    $validate_additional_forms = Html::escape($form_state->getValue('validate_additional_forms'));
    $values['validate_additional_forms'] = explode("\n", $validate_additional_forms);

    $editable = $this->configFactory()->getEditable(static::SETTINGS);

    foreach ($values as $name => $value) {
      $editable->set($name, $value);
      try {
        $editable->save();

        $this->messenger()->addMessage(
          $this->t('Configuration variable %variable was successfully saved.',
            ['%variable' => $name]
          )
        );

        $this->loggerChannel->info('Configuration variable %variable was successfully saved.',
          ['%variable' => $name]
        );
      }
      catch (\Exception $e) {
        $this->messenger()->addError($e->getMessage());
        $this->loggerChannel->error('Error saving configuration variable %variable : %error.',
          ['%variable' => $values['name'], '%error' => $e->getMessage()]);
      }
    }
  }

}
