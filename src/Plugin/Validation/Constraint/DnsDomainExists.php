<?php

namespace Drupal\dns_check\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is valid.
 *
 * A valid value is a domain that can be resolved or if an
 * email is submitted that it is a valid email format AND can be resolved.
 *
 * @Constraint(
 *   id = "DnsDomainExists",
 *   label = @Translation("DNS Domain exists", context = "Validation"),
 *   type = "string"
 * )
 */
class DnsDomainExists extends Constraint {

  /**
   * The value to return if a non-string value is passed.
   *
   * @var string
   */
  public $notString = "The %value is not a string.";

  /**
   * The value to return if an invalid email is passed.
   *
   * @var string
   */
  public $notValidEmail = "%value is not a properly formatted email.";

  /**
   * The value to return if a domain could not be resolved.
   *
   * @var string
   */
  public $notResolved = "The domain for %value could not be found. Check email spelling.";

}
