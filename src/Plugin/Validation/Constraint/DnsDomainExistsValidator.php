<?php

namespace Drupal\dns_check\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\dns_check\Service\DnsCheckerService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class DnsDomainExistsValidator.
 *
 * @package Drupal\dns_check\Plugin\Validation\Constraint
 */
class DnsDomainExistsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Use the checker service to test domains.
   *
   * @var \Drupal\dns_check\Service\DnsCheckerService
   */
  protected $checkerService;

  /**
   * DnsDomainExistsValidator constructor.
   *
   * @param \Drupal\dns_check\Service\DnsCheckerService $checkerService
   *   Use the checker service to validate.
   */
  public function __construct(DnsCheckerService $checkerService) {
    $this->checkerService = $checkerService;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('dns_check.dns_checker'));
  }

  /**
   * {@inheritDoc}
   */
  public function validate($value, Constraint $constraint) {

    // $value is \Drupal\Core\Field\FieldItemListInterface.
    if (!$constraint instanceof DnsDomainExists) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\\DnsDomainExists');
    }
    // Could be a multivalued field.
    foreach ($value->getValue() as $item) {
      $item = $item['value'];
      if (!is_string($item)) {
        $this->context->addViolation($constraint->notString, ['%value' => $item]);
        return;
      }
      if (mb_stristr($item, '@') !== FALSE) {
        $checkResult = $this->checkerService->validateEmail($item);
        if ($checkResult == $this->checkerService::INVALID_EMAIL) {
          $this->context->addViolation($constraint->notValidEmail, ['%value' => $item]);
        }
        elseif ($checkResult == $this->checkerService::NX_DOMAIN) {
          $this->context->addViolation($constraint->notResolved, ['%value' => $item]);
        }
      }
      else {
        $checkResult = $this->checkerService->validateHost($item);
        if ($checkResult == $this->checkerService::NX_DOMAIN) {
          $this->context->addViolation($constraint->notResolved, ['%value' => $item]);
        }
      }
    }
  }

}
